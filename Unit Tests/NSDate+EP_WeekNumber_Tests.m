#import <XCTest/XCTest.h>
#import "NSDate+EP_WeekNumber.h"

@interface NSDate_EP_WeekNumber_Tests : XCTestCase
@end

@implementation NSDate_EP_WeekNumber_Tests

- (void)testWeekNumberNowContainsLegalValue {
    NSInteger const weeksInYear = 53;
    NSInteger weekNumberToTest = [[NSDate weekNumberNow] integerValue];
    XCTAssertTrue(weekNumberToTest <= weeksInYear);
    XCTAssertTrue(weekNumberToTest >= 1);
}

- (void)testCorrectWeekFor31Dec2007 {
    NSDate *thirtyFirstDec2007 = [NSDate dateWithString:@"2007-12-31 12:00:00 +0000"];
    XCTAssertTrue([@"1" isEqualToString:[NSDate weekNumberForDate:thirtyFirstDec2007]]);
}

- (void)testCorrectWeekFor3Jan2010 {
    NSDate *thirdJan2010 = [NSDate dateWithString:@"2010-01-03 12:00:00 +0000"];
    XCTAssertTrue([@"53" isEqualToString:[NSDate weekNumberForDate:thirdJan2010]]);
}

@end
