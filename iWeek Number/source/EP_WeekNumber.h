#import <Foundation/Foundation.h>

@interface EP_WeekNumber : NSObject
+ (NSImage *)imageForCurrentWeekNumber;
+ (NSImage *)imageForWeekNumber:(NSInteger)number;
@end
