#import "NSDate+EP_WeekNumber.h"

@implementation NSDate (EP_WeekNumber)
+ (NSString *)weekNumberNow {
    return [NSDate weekNumberForDate:[NSDate date]];
}

+ (NSString *)weekNumberForDate:(NSDate *)targetDate {
    NSDateFormatter *weekFormat = [[NSDateFormatter alloc] init];
    [weekFormat setDateFormat:@"w"];
    return [weekFormat stringFromDate:targetDate];
}
@end
