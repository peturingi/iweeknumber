#import <Foundation/Foundation.h>

@interface NSDate (EP_WeekNumber)
+ (NSString *)weekNumberNow;
+ (NSString *)weekNumberForDate:(NSDate *)targetDate;
@end
