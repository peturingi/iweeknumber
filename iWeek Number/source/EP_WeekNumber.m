#import "EP_WeekNumber.h"
#import "NSDate+EP_WeekNumber.h"

@implementation EP_WeekNumber
+ (NSImage *)imageForCurrentWeekNumber {
    NSString *weekNumber = [NSDate weekNumberNow];
    return [self imageForWeekNumber:[weekNumber integerValue]];
}

+ (NSImage *)imageForWeekNumber:(NSInteger)number {
    NSImage *image = [NSImage imageNamed:[NSString stringWithFormat:@"%ld.png",number]];
    if (!image) @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Could not load image representation of a week number." userInfo:nil];
    return image;
}
@end
