#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    int exitCode;
    @try {
        exitCode = NSApplicationMain(argc, (const char **)argv);
    }
    @catch (NSException *e){
        NSLog(@"%@", e.reason);
        NSLog(@"%@", [NSThread callStackSymbols]);
    }
    @finally {
        return exitCode;
    }
}
