#import "AppControl.h"
#import "EP_WeekNumber.h"

NSTimeInterval const kSecondsBetweenUpdates;

@implementation AppControl {
    IBOutlet NSMenu *EP_statusMenu;
    NSStatusItem *EP_statusItem;
    NSTimer *weekNumberUpdater;
}

- (void)awakeFromNib {
    [self setupStatusBar];
    [self launchTimerWhichReactsToWeekChanges];
}

- (void)setupStatusBar {
    EP_statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [EP_statusItem setMenu:EP_statusMenu];
    [EP_statusItem setToolTip:@"iWeek"];
}

- (void)launchTimerWhichReactsToWeekChanges {
    weekNumberUpdater = [NSTimer scheduledTimerWithTimeInterval:kSecondsBetweenUpdates target:self selector:@selector(updateCurrentWeekNumberShownInStatusBar) userInfo:nil repeats:YES];
    [weekNumberUpdater fire];
}

- (void)updateCurrentWeekNumberShownInStatusBar {
    NSImage *weekNumberRepresentation = [EP_WeekNumber imageForCurrentWeekNumber];
    [EP_statusItem setImage:weekNumberRepresentation];
}

@end
